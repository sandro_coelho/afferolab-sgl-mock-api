# LEIAME #

### Iniciar a aplicação / instalação  

** Via docker ** 

docker run -it -p 8080:8080 sandroacoelho/afferolab-mock-api mockup-run.sh


### Via cURL

O aplicativo aceita content negotiation, porém só produz application/json :)

** Lista de Polos de Treinamento **

curl http://localhost:8080/trainning-place 

** Lista de Turmas **

curl http://localhost:8080/course-class 

** Lista de Turmas por página **

curl http://localhost:8080/course-class?page=3


** Lista de Turmas por data **

curl http://localhost:8080/course-class?startDate=16/01/2017


curl http://localhost:8080/course-class?startDate=16/01/2017&endDate=29/04/2017


** Lista de Turmas por página e polo **

curl http://localhost:8080/course-class?page=1&trainningPlaceId=Recife

** Lista de Presença **

curl http://localhost:8080/course-class/T1424650274689/attendance

** Avaliação dos Participantes **

curl http://localhost:8080/course-class/T1424650274689/questionare

** Cursos da Turma **

curl http://localhost:8080/course-class/T1424650274689/courses

### Documentação da API  [Working in progress]

http://localhost:8080