package br.com.afferolab.bo

import akka.actor.ActorRef
import spray.json.{JsonFormat, DefaultJsonProtocol}


package object sgl {

  type ResourceName = String

  case class Register(
                       name: ResourceName,
                       actorRef: ActorRef
                     )

  case class CourseClass(
                    id: String,
                    trainningRequest: String,
                    name: String,
                    trainningPlace: String,
                    startDate :String,
                    endDate : String
                  )

  case class Questionare(
                    sectionId: String,
                    sectionName: String,
                    questionId: String,
                    questionName: String
                  )

  case class Attendance(
                          id: String,
                          name: String,
                          cpf: String,
                          status: String,
                          justification: String
                        )

  case class Participant(
                          id: String,
                          cpf: String,
                          name: String,
                          status: String,
                          justification: String
                          )

  case class Course(
                    id: String,
                    name: String
                   )
  object JsonProtocols extends DefaultJsonProtocol {

    implicit val courseClass = jsonFormat6(CourseClass.apply)
    implicit val questionare = jsonFormat4(Questionare.apply)
    implicit val attendance = jsonFormat5(Attendance.apply)
    implicit val course = jsonFormat2(Course.apply)
  }


}
