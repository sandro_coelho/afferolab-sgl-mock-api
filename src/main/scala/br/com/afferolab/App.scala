package br.com.afferolab

import akka.actor.{ActorRefFactory, Actor, ActorSystem, Props}
import akka.io.IO
import akka.util.Timeout
import br.com.afferolab.bo.sgl.Register
import br.com.afferolab.endpoint.{Api, CourseClassService}
import spray.can.Http
import spray.http.{HttpRequest, HttpResponse, StatusCodes}
import spray.routing.{HttpService, HttpServiceActor}
import spray.util.SprayActorLogging

object Main extends App {
  implicit val system = ActorSystem()

  val master = system.actorOf(Props[ApiActor], name = "master")

  IO(Http) ! Http.Bind(master, interface = "0.0.0.0", port = 8080)

}

class ApiActor extends Actor with Api {

  override val actorRefFactory: ActorRefFactory = context

  def receive = runRoute(route)
}
