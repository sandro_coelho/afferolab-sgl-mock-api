package br.com.afferolab.endpoint


import java.util.{Calendar, Date, ResourceBundle}

import br.com.afferolab.bo.sgl.{Course, Attendance, CourseClass,Questionare}
import br.com.afferolab.bo.sgl.JsonProtocols._
import spray.http.MediaTypes._
import spray.json._
import spray.routing.{HttpService, RequestContext}

import scala.io.Source

trait CourseClassService extends HttpService  with CORSSupport {

  val courseClassRoute =
    cors {
      path("course-class") {
        get {
          respondWithMediaType(`application/json`) {
            parameterMap { params => {
              (ctx: RequestContext) => {
                ctx.complete {
                  val conf = ResourceBundle.getBundle(ctx.request.uri.path.tail.toString())
                  val page = Integer.valueOf(params.getOrElse("page", "0"))
                  val pageSize = Integer.valueOf(params.getOrElse("pageSize", "10"))
                  val pStartDate = params.getOrElse("startDate", "")
                  val pEndDate = params.getOrElse("endDate", "")



                  var result: List[CourseClass] = Source.fromFile(System.getProperty("user.home").concat("/").concat(conf.getString("path"))).getLines().map(c => {
                    new CourseClass(c.split(",")(0), c.split(",")(1), c.split(",")(2), c.split(",")(3), c.split(",")(4), c.split(",")(5))
                  }).toList

                  val trainningPlace = params.getOrElse("trainningPlaceId", "")

                  if (trainningPlace.length > 0) {
                    result = result.filter(p => p.trainningPlace equalsIgnoreCase trainningPlace)
                  }

                  val format = new java.text.SimpleDateFormat("dd/MM/yyyy")

                  var startDate: Date = null
                  var endDate: Date = null

                  try {
                    startDate = format.parse(pStartDate)

                    var cal = Calendar.getInstance()
                    cal.setTime(startDate)
                    cal.add(Calendar.DATE, -1)

                    startDate = cal.getTime()

                    println(startDate)

                    result = result.filter(p => format.parse(p.startDate) after startDate)

                    endDate = format.parse(pEndDate)
                    cal.setTime(endDate)
                    cal.add(Calendar.DATE, 1)

                    endDate = cal.getTime()
                    result = result.filter(p => format.parse(p.endDate) before endDate)
                  } catch {
                    case e: Exception => println("exception caught: " + e);
                  }


                  for (x <- 1 to page) {
                    result = result.drop(pageSize)
                  }

                  result.take(pageSize).toJson.toString()
                }
              }
            }
            }
          }
        }
      }
    }

}

trait CourseClassCourseService extends HttpService  with CORSSupport {

  val courseClassCourseRoute =
    cors {
      path("course-class" / Segment / "courses") { fileName =>
        get {
          respondWithMediaType(`application/json`) {
            parameterMap { params => {
              (ctx: RequestContext) => {
                ctx.complete {
                  val conf = ResourceBundle.getBundle("courses")
                  var result: List[Course] = Source.fromFile(System.getProperty("user.home").concat("/").concat(conf.getString("path"))).getLines().map(c => {
                    new Course(c.split(",")(1), c.split(",")(2))
                  }).toList

                  result.toJson.toString()
                }
              }
            }
            }
          }
        }
      }
    }
}

trait CourseClassAttendanceService extends HttpService with CORSSupport {

  val courseClassAttendanceRoute =
    cors {
      path("course-class" / Segment / "attendance") { fileName =>
        get {
          respondWithMediaType(`application/json`) {
            parameterMap { params => {
              (ctx: RequestContext) => {
                ctx.complete {
                  val conf = ResourceBundle.getBundle("attendance")
                  var result: List[Attendance] = Source.fromFile(System.getProperty("user.home").concat("/").concat(conf.getString("path"))).getLines().map(c => {
                    new Attendance(c.split(",")(0), c.split(",")(1), c.split(",")(2), "Y", "-")
                  }).toList

                  result.toJson.toString()
                }
              }
            }
            }
          }
        } ~
          put {
            complete {
              ""
            }
          }
      }
    }
}

trait CourseClassQuestionareService extends HttpService  with CORSSupport {

  val courseClassQuestionareRoute =
    cors {
      path("course-class" / Segment / "questionare") { fileName =>
        get {
          respondWithMediaType(`application/json`) {
            parameterMap { params => {
              (ctx: RequestContext) => {
                ctx.complete {
                  val conf = ResourceBundle.getBundle("questionare")
                  var result: List[Questionare] = Source.fromFile(System.getProperty("user.home").concat("/").concat(conf.getString("path"))).getLines().map(c => {
                    new Questionare(c.split(",")(0), c.split(",")(1), c.split(",")(2), c.split(",")(3))
                  }).toList

                  result.toJson.toString()
                }
              }
            }
            }
          }
        }
      }
    }
}


trait CourseClassEvaluationService extends HttpService  with CORSSupport{

  val courseClassEvaluationRoute =
    cors {
      path("course-class" / Segment / "evaluation") { fileName =>
        post {
          complete {
            ""
          }
        }
      }
    }
}
