package br.com.afferolab.endpoint


trait Api extends CourseClassService
  with CourseClassCourseService
  with CourseClassAttendanceService
  with CourseClassQuestionareService
  with TrainningPlaceService
  with CourseClassEvaluationService{
  val route = {
    courseClassRoute ~
      trainningPlaceRoute ~
      courseClassCourseRoute ~
      courseClassQuestionareRoute ~
      courseClassAttendanceRoute ~
      courseClassEvaluationRoute
    }
}
