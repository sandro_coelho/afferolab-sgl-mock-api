package br.com.afferolab.endpoint

import java.util.ResourceBundle

import spray.http.MediaTypes._
import spray.routing.{HttpService, RequestContext}


trait TrainningPlaceService extends HttpService  with CORSSupport{

  val trainningPlaceRoute = {
    cors {
      path("trainning-place") {
        get {
          respondWithMediaType(`application/json`) {
            parameterMap { params => {
              (ctx: RequestContext) => {
                ctx.complete {
                  val conf = ResourceBundle.getBundle(ctx.request.uri.path.tail.toString())
                  new String(conf.getString("value").getBytes("ISO-8859-1"), "UTF-8")
                }
              }
            }
            }
          }
        }
      }
    }
  }
}


